var config = {
	"domain": "vente-gesplan.dynu.net",
	"protocol": "https",
	"resourcesPath": "resources",
	"title": "Bienvenido a index.vente-gesplan.dynu.net",
	"items": [{
		"subdomain": "portainer",
		"img": "portainer.png",
		"title": "Portainer",
		"description": "Gestor de contenedores Docker"
	},{
		"subdomain": "traefik",
		"img": "traefik.png",
		"title": "Traefik",
		"description": "Dashboard de Traefik, proxy inverso dinámico de contenedores y balanceador de carga"
	},{
		"subdomain": "nifi",
		"img": "nifi.png",
		"title": "Apache NiFi",
		"description": "Plataforma de procesamiento y distribución de datos"
	},{
		"subdomain": "pgadmin",
		"img": "pgadmin.png",
		"title": "pgAdmin",
		"description": "Administrador para base de datos PostgreSQL"
	},{
		"subdomain": "jupyter",
		"img": "jupyter.png",
		"title": "Jupyter",
		"description": "Entorno Jupyter"
	}]
};
